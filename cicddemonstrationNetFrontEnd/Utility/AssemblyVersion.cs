﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SomeDemoCode.Std")]
[assembly: AssemblyDescription("This is an example of ci/cd project")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Nnamella")]
[assembly: AssemblyProduct("Nnamella 1.0")]
[assembly: AssemblyCopyright("Copyright © Nnamella")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


// Setting ComVisible to false makes the types in this assembly not visible
// to COM components. If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("CA7543D7-0F0F-4B48-9398-2712098E9324")]

