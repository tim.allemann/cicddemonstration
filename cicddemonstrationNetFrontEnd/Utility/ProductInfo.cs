﻿using System;
using System.IO;
using System.Reflection;

namespace cicddemonstrationNetFrontEnd.Utility
{
    // https://makolyte.com/auto-increment-build-numbers-in-visual-studio/
    // This is needed :
    /*
          <Import Project="$(MSBuildExtensionsPath)\Microsoft\VisualStudio\v16.0\TextTemplating\Microsoft.TextTemplating.targets" />

          <PropertyGroup>
            <GenerateAssemblyInfo>false</GenerateAssemblyInfo>
            <TransformOnBuild>true</TransformOnBuild>
            <OverwriteReadOnlyOutputFiles>true</OverwriteReadOnlyOutputFiles>
            <TransformOutOfDateOnly>false</TransformOutOfDateOnly>
          </PropertyGroup>

    for file VersionAutoIncrementer
    also look t4 section in csproject

     */
    // https://sachabarbs.wordpress.com/2020/02/23/net-core-standard-auto-incrementing-versioning/
    // You need : <GenerateAssemblyInfo>false</GenerateAssemblyInfo> in csproject
    // https://stackoverflow.com/questions/1600962/displaying-the-build-date/48102243#48102243
    // You need : <Deterministic>false</Deterministic> in csproject
    public static class ProductInfo
    {
        /// <summary>Gets the current OS running on.</summary>
        public static string OsPlatform() {
            string osPlatform = System.Runtime.InteropServices.RuntimeInformation.OSDescription;
            return osPlatform;
        }

        /// <summary>Gets the .net version.</summary>
        public static string GetAspDotnetVersion() {
           string aspDotnetVersion = Assembly
                                       .GetEntryAssembly()?
                                       .GetCustomAttribute<System.Runtime.Versioning.TargetFrameworkAttribute>()?
                                       .FrameworkName;
            return aspDotnetVersion;
        }
        
        /// <summary>Gets the appVersion Ex:1.0.1.122.</summary>
        public static string GetAppVersion()
        {
            string appversion = Assembly
                                    .GetEntryAssembly()?
                                    .GetCustomAttribute<System.Reflection.AssemblyFileVersionAttribute>()?
                                    .Version;
            return appversion;
        }

        /// <summary>Gets the modified date of the assembly file.</summary>
        public static string GetAppDate()
        {
            
            const int peHeaderOffset = 60;
            const int linkerTimestampOffset = 8;
            byte[] bytes = new byte[2048];
            using (FileStream file = new FileStream(Assembly.GetEntryAssembly().Location, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                file.Read(bytes, 0, bytes.Length);
            }
            Int32 headerPos = BitConverter.ToInt32(bytes, peHeaderOffset);
            Int32 secondsSince1970 = BitConverter.ToInt32(bytes, headerPos + linkerTimestampOffset);
            DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime dateTimeUTC = dt.AddSeconds(secondsSince1970);
            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(dateTimeUTC, TimeZoneInfo.Local);
            string applicationLastBuildTime = localTime.ToString("dd-MMM-yyyy hh:mm:sstt") + " " + TimeZoneInfo.Local.Id;
            return applicationLastBuildTime;
        }

    }
}
